#!/usr/bin/env pythoh3

'''
Program to produce a triangle with numbers
'''

import sys


def line(number: int):
    """Return a string corresponding to the line for number"""
    linea = str(number)*number
    return linea


def triangle(number: int):
    """Return a string corresponding to the triangle for number"""
    triangulo = ""
    for x in range (1, number+1):
        triangulo = triangulo + line(x) + "\n"
    return triangulo
def main():
    try:
        number: int = sys.argv[1]
        if 1 < int (number) < 9:
            text = triangle(int(number))
            print(text)
        else:
            print(triangle(9))
    except ValueError:
        print("Please provide a number (between 1 and 9) as argument")

if __name__ == '__main__':
    main()
